import requests as rq
import json
#<<<<<<< HEAD

def play_mastermind_game(mm_url, player_name, initial_guess, additional_guesses):
    register_url = mm_url + "register"
    create_url   = mm_url + "create"
    guess_url    = mm_url + "guess"

    register_dict = {"mode": "Mastermind", "name": player_name}

    session = rq.Session()

    res = session.post(register_url, json=register_dict)

    player_id = res.join()['id']
   
    create_dict = {"id", player_id, "overwrite": True}
    rc = session.post(create_url, json=create_dict)

    create_response = rc.json()
    if not create_response.get('created', False):
        raise Exception(f"Game creation failed: {create_response.get('message')}")
    print(create_response['message'])

    guess_dict = {"id": player_id, "guess": initial_guess}
    correct = session.post(guess_url, json=guess_dict)

    print(f"Guess '{initial_guess}': {correct.json()}")

    for guess in additional_gusses:
        guess_dict = {"id": player_id, "guess": additional_guess}
        correct = session.post(guess.url, json=guess_dict)
        

        print(f"Guess '{guess}': {correct.json()}")

def read_names_from_file(filename):
    with open(filename, 'r') as file:
        names = [name.strip() for name in file.readlines() if name.strip()]
    return names         


mm_url      = "https://we6.talentsprint.com/wordle/game/"
player_name = "WE_Duckiees"
initial_guess = read_names_from_file("5letters.txt")
additional_guess = read_names_from_file("5letters.txt")

    

<<<<<<< HEAD
=======

'''=======
>>>>>>> d7ce7c6f93ff5a3a52c138c9d21a0fda15b37732
print(rq.__version__)

def play_mastermind(player_name, filename):
    names = read_names_from_file(filename)

    register_data = {"mode": "Mastermind", "name": player_name}
    response = rq.post(register_url, json=register_data)

    if response.status_code != 201:
        print(f"Failed to register player {player_name}. Status code: {response.status_code}")
        return

    player_id = response.json().get('id')
    print(f"Player {player_name} registered with ID: {player_id}")

    create_data = {"id": player_id, "overwrite": True}
    response = rq.post(create_url, json=create_data)

    if response.status_code != 201:
        print(f"Failed to create game session for player {player_name}. Status code: {response.status_code}")
        return

    print(f"Game session created for player {player_name}")


    for name in names:
        guess_data = {"id": player_id, "guess": name.lower()}
        response = rq.post(guess_url, json=guess_data)

        if response.status_code != 200:
            print(f"Failed to make guess for player {player_name}. Status code: {response.status_code}")
            return

        feedback = response.json().get('feedback')
        message = response.json().get('message')

        print(f"Guess: {name}, Feedback: {feedback}, Message: {message}")

        if feedback == 5:
            print(f"WIN")
<<<<<<< HEAD
=======
>>>>>>> 1b4a9af8adfde7ab30624a69e5c2a60222c60b04'''
>>>>>>> d7ce7c6f93ff5a3a52c138c9d21a0fda15b37732
